package com.ivr.utilities.dto;

import java.util.Date;
import java.util.List;

public class GroupDTO {
  private Long groupId;
  private Long updatedUserId;
  private String updateUserName;
  private Long createdUserId;
  private String createUserName;
  private String groupName;
  private String groupNameNew;
  private String description;
  private Long parrentId;
  private Date createdDate;
  private Date updatedDate;
  private Integer status;
  private Boolean isParrent;
  private List<Long> groupUser;
  
  /**
   * 
   */
  public GroupDTO() {
    super();
  }
  /**
   * @param groupId
   * @param updatedUserId
   * @param updateUserName
   * @param createdUserId
   * @param createUserName
   * @param groupName
   * @param description
   * @param parrentId
   * @param createdDate
   * @param updatedDate
   * @param status
   */
  public GroupDTO(Long groupId, Long updatedUserId, String updateUserName, Long createdUserId, String createUserName,
      String groupName, String description, Long parrentId, Date createdDate, Date updatedDate,
      Integer status) {
    super();
    this.groupId = groupId;
    this.updatedUserId = updatedUserId;
    this.updateUserName = updateUserName;
    this.createdUserId = createdUserId;
    this.createUserName = createUserName;
    this.groupName = groupName;
    this.description = description;
    this.parrentId = parrentId;
    this.createdDate = createdDate;
    this.updatedDate = updatedDate;
    this.status = status;
  }
  
  public GroupDTO(Long groupId, Long parrentId, String groupName, Boolean isParrent) {
    super();
    this.groupId = groupId;
    this.parrentId = parrentId;
    this.groupName = groupName;
    this.isParrent = isParrent;
  }
  
  public GroupDTO(Long groupId, Long parrentId, String groupName, String description, Boolean isParrent) {
    super();
    this.groupId = groupId;
    this.parrentId = parrentId;
    this.groupName = groupName;
    this.isParrent = isParrent;
    this.description = description;
  }
  
  /**
   * @return the groupNameNew
   */
  public String getGroupNameNew() {
    return groupNameNew;
  }
  /**
   * @param groupNameNew the groupNameNew to set
   */
  public void setGroupNameNew(String groupNameNew) {
    this.groupNameNew = groupNameNew;
  }
  /**
   * @return the groupId
   */
  public Long getGroupId() {
    return groupId;
  }
  /**
   * @param groupId the groupId to set
   */
  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }
  /**
   * @return the updatedUserId
   */
  public Long getUpdatedUserId() {
    return updatedUserId;
  }
  /**
   * @param updatedUserId the updatedUserId to set
   */
  public void setUpdatedUserId(Long updatedUserId) {
    this.updatedUserId = updatedUserId;
  }
  /**
   * @return the updateUserName
   */
  public String getUpdateUserName() {
    return updateUserName;
  }
  /**
   * @param updateUserName the updateUserName to set
   */
  public void setUpdateUserName(String updateUserName) {
    this.updateUserName = updateUserName;
  }
  /**
   * @return the createdUserId
   */
  public Long getCreatedUserId() {
    return createdUserId;
  }
  /**
   * @param createdUserId the createdUserId to set
   */
  public void setCreatedUserId(Long createdUserId) {
    this.createdUserId = createdUserId;
  }
  /**
   * @return the createUserName
   */
  public String getCreateUserName() {
    return createUserName;
  }
  /**
   * @param createUserName the createUserName to set
   */
  public void setCreateUserName(String createUserName) {
    this.createUserName = createUserName;
  }
  /**
   * @return the groupName
   */
  public String getGroupName() {
    return groupName;
  }
  /**
   * @param groupName the groupName to set
   */
  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }
  /**
   * @return the description
   */
  public String getDescription() {
    return description;
  }
  /**
   * @param description the description to set
   */
  public void setDescription(String description) {
    this.description = description;
  }
  /**
   * @return the parrentId
   */
  public Long getParrentId() {
    return parrentId;
  }
  /**
   * @param parrentId the parrentId to set
   */
  public void setParrentId(Long parrentId) {
    this.parrentId = parrentId;
  }
  /**
   * @return the createdDate
   */
  public Date getCreatedDate() {
    return createdDate;
  }
  /**
   * @param createdDate the createdDate to set
   */
  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }
  /**
   * @return the updatedDate
   */
  public Date getUpdatedDate() {
    return updatedDate;
  }
  /**
   * @param updatedDate the updatedDate to set
   */
  public void setUpdatedDate(Date updatedDate) {
    this.updatedDate = updatedDate;
  }
  /**
   * @return the status
   */
  public Integer getStatus() {
    return status;
  }
  /**
   * @param status the status to set
   */
  public void setStatus(Integer status) {
    this.status = status;
  }
  /**
   * @return the isParrent
   */
  public Boolean getIsParrent() {
    return isParrent;
  }
  /**
   * @param isParrent the isParrent to set
   */
  public void setIsParrent(Boolean isParrent) {
    this.isParrent = isParrent;
  }
  /**
   * @return the groupUser
   */
  public List<Long> getGroupUser() {
    return groupUser;
  }
  /**
   * @param groupUser the groupUser to set
   */
  public void setGroupUser(List<Long> groupUser) {
    this.groupUser = groupUser;
  }
}
