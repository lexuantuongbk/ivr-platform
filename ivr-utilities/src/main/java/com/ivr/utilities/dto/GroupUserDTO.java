package com.ivr.utilities.dto;

import java.io.Serializable;
import java.util.List;

public class GroupUserDTO implements Serializable {

	/**
   * 
   */
	private static final long serialVersionUID = 6175062225916642351L;

	private Long groupId;
	private String groupName;
	private Long userId;
	private String userName;
	private List<Long> userIds;

	/**
   * 
   */
	public GroupUserDTO() {
		super();
	}

	/**
	 * @param groupId
	 * @param userId
	 * @param userName
	 * @param createDate
	 * @param createUserId
	 * @param createUserName
	 */
	public GroupUserDTO(Long groupId, String groupName, Long userId,
			String userName) {
		super();
		this.groupId = groupId;
		this.groupName = groupName;
		this.userId = userId;
		this.userName = userName;
	}

	/**
	 * @return the groupId
	 */
	public Long getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId
	 *            the groupId to set
	 */
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName
	 *            the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the userIds
	 */
	public List<Long> getUserIds() {
		return userIds;
	}

	/**
	 * @param userIds
	 *            the userIds to set
	 */
	public void setUserIds(List<Long> userIds) {
		this.userIds = userIds;
	}
}
