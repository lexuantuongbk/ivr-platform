package com.ivr.utilities.dto;

import java.util.Date;

public class PasswordHistoryDTO {
	private Long passwordHistoryId;
	private Long userId;
	private Date createdDate;
	private String password;

	public PasswordHistoryDTO(Long passwordHistoryId, Long userId,
			Date createdDate, String password) {
		this.passwordHistoryId = passwordHistoryId;
		this.userId = userId;
		this.createdDate = createdDate;
		this.password = password;
	}

	public PasswordHistoryDTO() {

	}

	/**
	 * @return the passwordHistoryId
	 */
	public Long getPasswordHistoryId() {
		return passwordHistoryId;
	}

	/**
	 * @param passwordHistoryId
	 *            the passwordHistoryId to set
	 */
	public void setPasswordHistoryId(Long passwordHistoryId) {
		this.passwordHistoryId = passwordHistoryId;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate
	 *            the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
