/**
 * 
 */
package com.ivr.utilities;

public class Constants {
  
  public static final String EPOS_WARRING = "WARRING";
  
  public static final String EPOS_SUCCESS = "SUCCESS";
  
  public static final String EPOS_FALSE = "FALSE";
  
  public static final String EPOS_EXISTED = "EXISTED";
  
  public static final String EPOS_IPADDRESS_DETAIL_EXISTED = "DETAILEXISTED";
  
  public static final String EMAIL_EXISTED = "EMAIL_EXISTED";
  
  public static final String EPOS_NOT_EXIST = "NOT_EXIST";
  
  public static final String EPOS_ISNULL = "ISNULL";
  
  public static final String EPOS_INVALIDATE = "INVALIDATE";
  
  public static final Integer EPOS_DELETED = 1 ;
  
  public static final Integer EPOS_NOT_DELETED = 0 ;
  
  public static final Integer USER_STATUS_IN_ACTIVE =  0;  
  
  public static final Integer USER_STATUS_ACTIVE = 1;
  
  public static final Integer USER_STATUS_BLOCK = 2;
  
  public static final String EPOS_GROUP_USER_EXISTED = "EPOS_GROUP_USER_EXISTED";
  
  public static final String EPOS_GROUP_MODULE_EXISTED = "EPOS_GROUP_MODULE_EXISTED";
  
  public static final String SORT_BY_USER_NAME = "USER_NAME";
  
  public static final String SORT_BY_CREATE_DATE = "CREATE_DATE";
  
  public static final String SORT_BY_USER_CREATED = "USER_CREATED";
  
  public static final String SORT_BY_GROUP_NAME = "GROUP_NAME";
  
  public static final String SORT_BY_PERMISSION_NAME = "PERMISSION_NAME";
  
  public static final String SORT_BY_MODULE_NAME = "MODULE_NAME";
  
  public static final String EPOS_NOT_DELETE = "NOT_DELETE";
  
  public static final String EPOS_NOT_UPDATE = "NOT_UPDATE";
  
  public static final String SORT_BY_SCHEDULE_NAME = "SCHEDULE_NAME";
  
  public static final String SORT_BY_IP_ADDRESS_NAME = "IP_ADDRESS_NAME";
  
  public static final String SORT_BY_IP_ADDRESS_VALUE = "IP_ADDRESS_VALUE";
  /**
   * TÃªn trÆ°á»�ng láº¥y sá»‘ ngÃ y háº¿t háº¡n cá»§a password
   */
  public static final String POLICY_NAME_PASSWORD_EXPIRY_DAY = "PASSWORD_EXPIRY_DAY";
  
  /**
   * TÃªn trÆ°á»�ng láº¥y sá»‘ láº§n nháº­p pass
   */
  public static final String POLICY_NAME_NUM_OF_PASSWORD_HISTORY_NOT_ALLOW = "NUM_OF_PASSWORD_HISTORY_NOT_ALLOW";
  
  /**
   * Password bá»‹ trÃ¹ng
   */
  public static final String DUPLICATE_PASSWORD = "DUPLICATE_PASSWORD";
  
  public static final String IP_PUBLIC = "PUBLIC";
  /**
   * Permisison   */
  public static final String EPOS_UMS_PERMISSION_NAME = "EPOS_UMS_PERMISSION_NAME";
  public static final String EPOS_UMS_PERMISSION_CODE = "EPOS_UMS_PERMISSION_CODE";
  
  /**
   * 1: Ip value day khong ton tai
   */
  public static final int LOGIN_STATUS_IP_VALUE_NOT_EXISTED = 1;
  /**
   * 2: ip day bi cam khong dc phep
   */
  public static final int LOGIN_STATUS_IP_IS_RESTRICT = 2;
  /**
   * 3 : duoc phep login
   */
  public static final int LOGIN_STATUS_SUCCESS = 3;
  /**
   * 4 : user khong co lich schedule
   */
  public static final int LOGIN_STATUS_USER_DO_NOT_HAVE_SCHEDULE = 4;

  /**
   *1: cho phep 
   */
  public static final int ACCESS_TYPE_ACCEPT = 1;
  /**
   * 2 : khong cho phep 
   */
  public static final int ACCESS_TYPE_NOT_ACCEPT = 2;
  /**
   * 3: bo chon
   */
  public static final int ACCESS_TYPE_NOT_CHOOSE = 3;
}
