/**
 * 
 */
package com.ivr.api.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ivr.api.model.User;
import com.ivr.api.service.UserService;
import com.ivr.utilities.dto.SearchCriterialUserDTO;
import com.ivr.utilities.dto.UserDTO;

@Controller
@RequestMapping("platform/user")
public class UserController {
  
  @Autowired
  private UserService userService;
  
  @RequestMapping(value = "/create-user", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public String createUser(@RequestBody UserDTO userDTO)  {
      return userService.createUser(userDTO);
  }
  
  @RequestMapping(value = "/update-user", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public String updateUser(@RequestBody UserDTO userDTO) {
    return userService.updateUser(userDTO);
  }
  
  @RequestMapping(value = "/delete-user/{userId}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public String deleteUser(@PathVariable("userId")Long userId) throws JsonParseException, JsonMappingException,
      UnsupportedEncodingException, IOException {
    return userService.deleteUser(userId);
  }
  
  @RequestMapping(value = "/find-user/{criterialUserDTO}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public List<UserDTO> findUser(@PathVariable("criterialUserDTO")String criterialUserDTO) throws JsonParseException, JsonMappingException,
      UnsupportedEncodingException, IOException {
    ObjectMapper mapper = new ObjectMapper();
    criterialUserDTO = criterialUserDTO.replace(".", "%2E");
    SearchCriterialUserDTO returnValue = mapper.readValue(URLDecoder.decode(criterialUserDTO, "UTF-8"), SearchCriterialUserDTO.class);
    return userService.findUser(returnValue);
  }
  
  @RequestMapping(value = "/count-find-user/{criterialUserDTO}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public Long countFindUser(@PathVariable("criterialUserDTO")String criterialUserDTO) throws JsonParseException, JsonMappingException,
      UnsupportedEncodingException, IOException {
    ObjectMapper mapper = new ObjectMapper();
    criterialUserDTO = criterialUserDTO.replace(".", "%2E");
    SearchCriterialUserDTO returnValue = mapper.readValue(URLDecoder.decode(criterialUserDTO, "UTF-8"), SearchCriterialUserDTO.class);
    return userService.countUser(returnValue);
  }
  
  @RequestMapping(value = "/update-status-user/{userId}/{status}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public String updateStatusUser(@PathVariable("userId")Long userId,@PathVariable("status") Integer status) {
    return userService.updateStatusUser(userId, status);
  }
  
  @RequestMapping(value = "/get-user-by-username/{userName}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public User getUserByUserName(@PathVariable("userName")String userName) {
    return userService.getUserByUserName(userName);
  }
  
  @RequestMapping(value = "/get-user-by-id/{userId}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public UserDTO getUserById(@PathVariable("userId")Long userId) {
    return userService.getUserById(userId);
  }
  
  @RequestMapping(value = "/reset-password/{userId}/{passNew}", 
      method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public String resetPassword(@PathVariable("userId")Long userId,@PathVariable("passNew") String passNew) {
    return userService.resetPassword(userId, passNew);
  }
  

  
}