package com.ivr.api.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.ivr.api.model.User;
import com.ivr.api.repository.custom.UserRepositoryCustom;

public interface UserRepository extends CrudRepository<User, Long>,
		UserRepositoryCustom {

	@Query(" SELECT u FROM User u WHERE UPPER(u.userName) = UPPER(:userName) ")
	public User checkUserExisted(@Param("userName") String userName);

	@Query(" SELECT COUNT(u.userId) FROM User u WHERE u.email = :email")
	public Long checkEmailExisted(@Param("email") String email);

}
