/**
 * 
 */
package com.ivr.api.repository.custom;

import java.util.List;

import com.ivr.utilities.dto.SearchCriterialUserDTO;
import com.ivr.utilities.dto.UserDTO;


public interface UserRepositoryCustom {
  
  public List<UserDTO> findUser(SearchCriterialUserDTO criterialUserDTO);
  
  public Long countFindUser(SearchCriterialUserDTO criterialUserDTO);
  
}
