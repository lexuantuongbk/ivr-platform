package com.ivr.api.repository.custom;

import java.util.List;

import com.ivr.utilities.dto.PasswordHistoryDTO;


public interface PasswordHistoryRepositoryCustom {

  public Long checkDuplicatePassword(Long userId,Integer numberPass,String password);
  
  public List<PasswordHistoryDTO> getLimitPasswordHistoryByUserId(int litmit ,Long userId);
}
