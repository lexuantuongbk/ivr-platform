/**
 * 
 */
package com.ivr.api.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;

import com.ivr.api.repository.custom.UserRepositoryCustom;
import com.ivr.utilities.Constants;
import com.ivr.utilities.dto.SearchCriterialUserDTO;
import com.ivr.utilities.dto.UserDTO;

public class UserRepositoryImpl implements UserRepositoryCustom {

  @PersistenceContext
  private EntityManager entityManager;

  @SuppressWarnings("unchecked")
  @Override
  public List<UserDTO> findUser(SearchCriterialUserDTO criterialUserDTO) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(" SELECT new com.htc.platform.dto.UserDTO( ");
    stringBuilder.append(" u.userId ,u.userName, u.password, u.email, u.createdDate,  ");
    stringBuilder.append(" u.lastChangedPassword, u.failedPasswordDate, u.failedPasswordCount, u.lastBlockDate, ");
    stringBuilder.append(" u.lastLoginDate, u.fullName, u.birthday, ");
    stringBuilder.append(" u.address, u.mobilePhone, u.company, ");
    stringBuilder.append(" u.fax, u.lastUpdatedDate, u.expiryDate, ");
    stringBuilder.append(" u.passwordExpiryStatus, u.pirority, ");
    stringBuilder.append(" u.status, u.gender , u.userCreated , ");
    stringBuilder.append(" (SELECT us.userName FROM User us WHERE us.userId = u.userCreated) ");
    stringBuilder.append(" ) ");
    stringBuilder.append(" FROM User u WHERE 1 = 1 ");
    if (StringUtils.isNotBlank(criterialUserDTO.getUserName())) {
      stringBuilder.append(" AND UPPER(u.userName) LIKE UPPER(:userName) ");
    }
    if (criterialUserDTO.getStatus() != null) {
      stringBuilder.append(" AND u.status = :status ");
    }
    if (criterialUserDTO.getFromDate() != null && criterialUserDTO.getToDate() != null) {
      stringBuilder.append(" AND u.createdDate BETWEEN :fromDate AND :toDate ");
    } else {
      if (criterialUserDTO.getFromDate() != null) {
        stringBuilder.append(" AND u.createdDate >= :fromDate ");
      }
      if (criterialUserDTO.getToDate() != null) {
        stringBuilder.append(" AND u.createdDate <= :toDate ");
      }
    }
    if (!StringUtils.isEmpty(criterialUserDTO.getSortField())) {
      stringBuilder.append(" ORDER BY ");
      if (Constants.SORT_BY_USER_NAME.equals(criterialUserDTO.getSortField().trim().toUpperCase())) {
        stringBuilder.append(" u.userName ");
      } else if (Constants.SORT_BY_CREATE_DATE.equals(criterialUserDTO.getSortField().trim().toUpperCase())) {
        stringBuilder.append(" u.createdDate ");
      } else if (Constants.SORT_BY_USER_CREATED.equals(criterialUserDTO.getSortField().trim().toUpperCase())) {
        stringBuilder.append(" u.userCreated ");
      }
      stringBuilder.append(criterialUserDTO.getSortOrder());
    } else {
      stringBuilder.append(" ORDER BY u.userId DESC ");
    }
    Query query = entityManager.createQuery(stringBuilder.toString());
    if (StringUtils.isNotBlank(criterialUserDTO.getUserName())) {
      query.setParameter("userName", "%" + criterialUserDTO.getUserName().trim() + "%");
    }
    if (criterialUserDTO.getStatus() != null ) {
      query.setParameter("status", criterialUserDTO.getStatus());
    }
    if (criterialUserDTO.getFromDate() != null) {
      // query.setParameter("fromDate", criterialUserDTO.getFromDate());
      query.setParameter("fromDate", DateUtils.toCalendar(criterialUserDTO.getFromDate()), TemporalType.DATE);
    }
    if (criterialUserDTO.getToDate() != null) {
      // query.setParameter("toDate", criterialUserDTO.getToDate());
      query.setParameter("toDate", DateUtils.toCalendar(criterialUserDTO.getToDate()), TemporalType.DATE);
    }
    query.setFirstResult(criterialUserDTO.getPageIndex());
    query.setMaxResults(criterialUserDTO.getPageSize());
    return query.getResultList();
  }

  @Override
  public Long countFindUser(SearchCriterialUserDTO criterialUserDTO) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(" SELECT COUNT(u.userId) ");
    stringBuilder.append(" FROM User u WHERE 1 = 1 ");
    if (StringUtils.isNotBlank(criterialUserDTO.getUserName())) {
      stringBuilder.append(" AND UPPER(u.userName) LIKE UPPER(:userName) ");
    }
    if (criterialUserDTO.getStatus() != null ) {
      stringBuilder.append(" AND u.status = :status ");
    }
    if (criterialUserDTO.getFromDate() != null && criterialUserDTO.getToDate() != null) {
      stringBuilder.append(" AND u.createdDate BETWEEN :fromDate AND :toDate ");
    } else {
      if (criterialUserDTO.getFromDate() != null) {
        stringBuilder.append(" AND u.createdDate >= :fromDate ");
      }
      if (criterialUserDTO.getToDate() != null) {
        stringBuilder.append(" AND u.createdDate <= :toDate ");
      }
    }
    Query query = entityManager.createQuery(stringBuilder.toString());
    if (StringUtils.isNotBlank(criterialUserDTO.getUserName())) {
      query.setParameter("userName", "%" + criterialUserDTO.getUserName().trim() + "%");
    }
    if (criterialUserDTO.getStatus() != null ) {
      query.setParameter("status", criterialUserDTO.getStatus());
    }
    if (criterialUserDTO.getFromDate() != null) {
      // query.setParameter("fromDate", criterialUserDTO.getFromDate());
      query.setParameter("fromDate", DateUtils.toCalendar(criterialUserDTO.getFromDate()), TemporalType.DATE);
    }
    if (criterialUserDTO.getToDate() != null) {
      // query.setParameter("toDate", criterialUserDTO.getToDate());
      query.setParameter("toDate", DateUtils.toCalendar(criterialUserDTO.getToDate()), TemporalType.DATE);
    }
    return (Long) query.getSingleResult();
  }

}
