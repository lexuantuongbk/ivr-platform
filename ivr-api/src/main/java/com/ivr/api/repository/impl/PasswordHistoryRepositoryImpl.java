/**
 * 
 */
package com.ivr.api.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;

import com.ivr.api.repository.custom.PasswordHistoryRepositoryCustom;
import com.ivr.utilities.dto.PasswordHistoryDTO;

public class PasswordHistoryRepositoryImpl implements PasswordHistoryRepositoryCustom {

  @PersistenceContext
  EntityManager entityManager;
  
  @Override
  public Long checkDuplicatePassword(Long userId, Integer numberPass, String password) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(" SELECT COUNT(ph2.passwordHistory_Id) ");
    stringBuilder.append(" FROM ( SELECT * FROM ");
    stringBuilder.append(" (  SELECT * FROM PasswordHistory ph ");
    stringBuilder.append(" WHERE ph.user_Id = :userId ORDER BY ph.created_Date DESC ");
    stringBuilder.append(" ) ");
    stringBuilder.append(" ph1  WHERE  ROWNUM <= :numberPass ");
    stringBuilder.append(" )  ph2  WHERE UPPER(ph2.password) LIKE UPPER(:password) ");
    
    SQLQuery query = entityManager.unwrap(Session.class).createSQLQuery(stringBuilder.toString());
    query.setParameter("userId", userId);
    query.setParameter("numberPass", numberPass);
    query.setParameter("password", password.trim());
    return Long.valueOf(String.valueOf(query.uniqueResult()));
  }

  @Override
  public List<PasswordHistoryDTO> getLimitPasswordHistoryByUserId(int litmit, Long userId) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(" SELECT ph2.PASSWORDHISTORY_ID AS passwordHistoryId , ");
    stringBuilder.append(" ph2.PASSWORD AS password , ph2.USER_ID AS userId , ph2.CREATED_DATE AS createdDate  ");
    stringBuilder.append(" FROM ( SELECT * FROM ");
    stringBuilder.append(" (  SELECT * FROM PASSWORD_HISTORY ph ");
    stringBuilder.append(" WHERE ph.user_Id = :userId ORDER BY ph.created_Date DESC ");
    stringBuilder.append(" ) ");
    stringBuilder.append(" ph1  WHERE  ROWNUM <= :litmit ");
    stringBuilder.append(" )  ph2  ");
    
    SQLQuery query = entityManager.unwrap(Session.class).createSQLQuery(stringBuilder.toString());
    query.addScalar("passwordHistoryId", StandardBasicTypes.LONG).addScalar("password", StandardBasicTypes.STRING)
    .addScalar("userId", StandardBasicTypes.LONG).addScalar("createdDate", StandardBasicTypes.DATE);
    query.setResultTransformer(Transformers.aliasToBean(PasswordHistoryDTO.class));
    query.setParameter("userId", userId);
    query.setParameter("litmit", litmit);
    return query.list();
  }

}
