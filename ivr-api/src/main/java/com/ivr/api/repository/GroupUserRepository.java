package com.ivr.api.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.ivr.api.model.GroupUser;
import com.ivr.api.model.GroupUserId;
import com.ivr.api.repository.custom.GroupUserRepositoryCustom;

public interface GroupUserRepository extends CrudRepository<GroupUser, GroupUserId> ,GroupUserRepositoryCustom {
  
  @Query("SELECT COUNT(id.groupId) FROM GroupUser WHERE id.groupId = :groupId")
  Long checkExistGroup(@Param("groupId") Long groupId);
  
  @Modifying
  @Transactional
  @Query(" DELETE FROM GroupUser gu WHERE gu.id.groupId = :groupId ")
  public int deleteAllByGourpId(@Param("groupId")Long groupId);
}
