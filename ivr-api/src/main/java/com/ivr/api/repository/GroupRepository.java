package com.ivr.api.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.ivr.api.model.Groups;
import com.ivr.api.repository.custom.GroupRepositoryCustom;

public interface GroupRepository extends CrudRepository<Groups, Long>,
		GroupRepositoryCustom {

	@Query(" SELECT g FROM Groups g WHERE upper(g.groupName) = upper(:groupName) ")
	Groups checkExisted(@Param("groupName") String groupName);

}
