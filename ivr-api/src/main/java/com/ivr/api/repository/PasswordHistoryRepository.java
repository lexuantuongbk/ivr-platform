package com.ivr.api.repository;

import org.springframework.data.repository.CrudRepository;

import com.ivr.api.model.PasswordHistory;
import com.ivr.api.repository.custom.PasswordHistoryRepositoryCustom;

public interface PasswordHistoryRepository extends CrudRepository<PasswordHistory, Long> ,PasswordHistoryRepositoryCustom {
}
