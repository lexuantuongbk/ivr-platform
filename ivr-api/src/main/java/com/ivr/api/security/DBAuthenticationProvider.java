package com.ivr.api.security;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.ivr.api.model.User;
import com.ivr.api.service.UserService;


@Component("dbAuthenticationProvider")
public class DBAuthenticationProvider implements AuthenticationProvider {

  @Autowired
  private UserService userService;

  /*
   * (non-Javadoc)
   * 
   * @see
   * org.springframework.security.authentication.AuthenticationProvider#authenticate
   * (org.springframework.security.core.Authentication)
   */
  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    final String username = (String) authentication.getPrincipal();
    final String rawPassword = (String) authentication.getCredentials();

    if (StringUtils.isBlank(username) || StringUtils.isBlank(rawPassword)) {
      throw new AuthenticationException("Username or password is blank") {
        private static final long serialVersionUID = 5179054786813051368L;
      };
    }

    User dbUser = null;
    try {
      dbUser = userService.verify(username, rawPassword);
    } catch (UsernameNotFoundException e) {
      throw new AuthenticationException(e.getMessage(), e) {
        private static final long serialVersionUID = 5376607537697316299L;
      };
    }
    if (dbUser != null) {
      List<GrantedAuthority> roles = AuthorityUtils.NO_AUTHORITIES;
      org.springframework.security.core.userdetails.User user = new org.springframework.security.core.userdetails.User(
          username, rawPassword, roles);
      UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(user, null, roles);

      authToken.setDetails(dbUser);
      return authToken;
    }
    return null;
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return true;
  }
}
