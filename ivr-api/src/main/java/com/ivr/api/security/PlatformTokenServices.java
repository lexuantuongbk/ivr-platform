package com.ivr.api.security;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;

import com.ivr.api.model.User;


public class PlatformTokenServices extends DefaultTokenServices {

  private static final String USER_ID = "user_id";
  private static final String FULLNAME = "fullname";

  @Override
  public OAuth2AccessToken createAccessToken(OAuth2Authentication authentication) throws AuthenticationException {
    OAuth2AccessToken accessToken = super.createAccessToken(authentication);
    addInformation(authentication, accessToken);
    return accessToken;
  }

  @Override
  public OAuth2AccessToken refreshAccessToken(String refreshTokenValue, AuthorizationRequest request)
      throws AuthenticationException {
    OAuth2AccessToken accessToken = super.refreshAccessToken(refreshTokenValue, request);
    OAuth2Authentication authentication = super.loadAuthentication(accessToken.getValue());
    addInformation(authentication, accessToken);
    return accessToken;
  }

  /**
   * Add user_id and fullname to the token as additional information
   * 
   * @param authentication
   * @param accessToken
   */
  private void addInformation(OAuth2Authentication authentication, OAuth2AccessToken accessToken) {
    if ((accessToken instanceof DefaultOAuth2AccessToken)
        && authentication.getUserAuthentication() != null
        && (authentication.getUserAuthentication().getDetails() instanceof User)) {
      DefaultOAuth2AccessToken htcAccessToken = (DefaultOAuth2AccessToken) accessToken;
      Map<String, Object> info = new HashMap<String, Object>();
      User user = (User) authentication.getUserAuthentication().getDetails();
      info.put(USER_ID, user.getUserId());
      info.put(FULLNAME, user.getFullName());
      htcAccessToken.setAdditionalInformation(info);
    }
  }
}
