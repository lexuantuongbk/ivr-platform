package com.ivr.api.security;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

class PlatformWebAuthenticationDetails extends WebAuthenticationDetails {

  private static final long serialVersionUID = -3426582485118546176L;
  private static final String APP_ID_REQUEST_PARAM = "app_id";
  private static final int DEFAULT_APP_ID = -1;

  private int appId;

  public PlatformWebAuthenticationDetails(HttpServletRequest request) {
    super(request);
    this.appId = NumberUtils.toInt(request.getParameter(APP_ID_REQUEST_PARAM), DEFAULT_APP_ID);
  }

  public int getAppId() {
    return appId;
  }
}
