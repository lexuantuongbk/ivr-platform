package com.ivr.api.security;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

@Component("platformWebAuthenticationDetailsSource")
public class PlatformWebAuthenticationDetailsSource implements
    AuthenticationDetailsSource<HttpServletRequest, WebAuthenticationDetails> {

  /* (non-Javadoc)
   * @see org.springframework.security.authentication.AuthenticationDetailsSource#buildDetails(java.lang.Object)
   */
  @Override
  public WebAuthenticationDetails buildDetails(HttpServletRequest context) {
    return new PlatformWebAuthenticationDetails(context);
  }

}
