package com.ivr.api.service;

import java.util.List;

import com.ivr.api.model.User;
import com.ivr.utilities.dto.SearchCriterialUserDTO;
import com.ivr.utilities.dto.UserDTO;

public interface UserService {

  String createUser(UserDTO userDTO);

  String updateUser(UserDTO userDTO);

  String deleteUser(Long userId);

  List<UserDTO> findUser(SearchCriterialUserDTO criterialUserDTO);

  Long countUser(SearchCriterialUserDTO criterialUserDTO);
  
  String updateStatusUser(Long userId , Integer status);
  
  User getUserByUserName(String userName);
  
  UserDTO getUserById(Long userId);
  
  String resetPassword(Long userId , String passNew);
  

  /**
   * Check whether user and password are valid or not.
   *
   * @param username user name to verify.
   * @param password a raw password that matches with encoded password in database.
   * @return An existing user.
   * @throws UsernameNotFoundException if if user name is not existing or password doesn't match.
   */
  User verify(String username, String password);

}
