/**
 * 
 */
package com.ivr.api.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ivr.api.model.GroupUser;
import com.ivr.api.model.GroupUserId;
import com.ivr.api.model.PasswordHistory;
import com.ivr.api.model.User;
import com.ivr.api.repository.GroupUserRepository;
import com.ivr.api.repository.PasswordHistoryRepository;
import com.ivr.api.repository.UserRepository;
import com.ivr.api.service.UserService;
import com.ivr.utilities.Constants;
import com.ivr.utilities.dto.SearchCriterialUserDTO;
import com.ivr.utilities.dto.UserDTO;

@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private UserRepository userRepository;


  @Autowired
  private PasswordHistoryRepository passwordHistoryRepository;
  
  @Autowired private GroupUserRepository groupUserRepository;

  public static void createOrUpdateUser(User user, UserDTO userDTO) {
    user.setAddress(userDTO.getAddress());
    user.setBirthday(userDTO.getBirthday());
    user.setCompany(StringUtils.trimToEmpty(userDTO.getCompany()));
    user.setCreatedDate(new Date());
    user.setEmail(StringUtils.trimToEmpty(userDTO.getEmail()));
    user.setExpiryDate(userDTO.getExpiryDate());
    user.setFailedPasswordCount(userDTO.getFailedPasswordCount());
    user.setFailedPasswordDate(userDTO.getFailedPasswordDate());
    
    user.setFax(StringUtils.trimToEmpty(userDTO.getFax()));
    user.setFullName(StringUtils.trimToEmpty(userDTO.getFullName()));
    user.setGender(userDTO.getGender());
    user.setLastBlockDate(userDTO.getLastBlockDate());
    user.setLastChangedPassword(userDTO.getLastChangedPassword());
    user.setLastLoginDate(userDTO.getLastLoginDate());
    user.setLastUpdatedDate(userDTO.getLastUpdatedDate());
    user.setMobilePhone(userDTO.getMobilePhone());
    user.setPassword(StringUtils.trimToEmpty(userDTO.getPassword()));
    user.setPasswordExpiryStatus(userDTO.getPasswordExpiryStatus());
    user.setPirority(userDTO.getPirority());
    user.setStatus(userDTO.getStatus());
    user.setUserCreated(userDTO.getUserCreated());
    user.setUserName(StringUtils.trimToEmpty(userDTO.getUserName()));
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = Throwable.class)
  public String createUser(UserDTO userDTO) {
    User userCheckExisted = userRepository.checkUserExisted(userDTO.getUserName().trim());
    if (userCheckExisted != null) {
      return Constants.EPOS_EXISTED;
    }
    Long countEmail = userRepository.checkEmailExisted(userDTO.getEmail().trim());
    if (countEmail > 0) {
      return Constants.EMAIL_EXISTED;
    }
    if (userDTO.getGroupId() == null || userDTO.getGroupId() == 0) {
      return Constants.EPOS_FALSE;
    }
    User user = new User();
    createOrUpdateUser(user, userDTO);

    User userSave = userRepository.save(user);

    PasswordHistory passwordHistory = new PasswordHistory();
    passwordHistory.setCreatedDate(Calendar.getInstance().getTime());
    passwordHistory.setPassword(userSave.getPassword());
    passwordHistory.setUsers(userSave);
    passwordHistoryRepository.save(passwordHistory);
    
    // assign user to group
    GroupUser groupUser = new GroupUser();
    GroupUserId id = new GroupUserId();
    id.setUserId(userSave.getUserId());
    id.setGroupId(userDTO.getGroupId());
    groupUser.setId(id);
    groupUserRepository.save(groupUser);

    return Constants.EPOS_SUCCESS;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = Throwable.class)
  public String updateUser(UserDTO userDTO) {
    //Lay thong tin user can update
    User user = userRepository.findOne(userDTO.getUserId());
    if (user != null) {
      //Kiem tra su duy nhat cua user
      if (!user.getEmail().equalsIgnoreCase(userDTO.getEmail().trim())) {
        Long countEmail = userRepository.checkEmailExisted(userDTO.getEmail().trim());
        if (countEmail > 0) {
          return Constants.EMAIL_EXISTED;
        }
      }

      String passTemp = user.getPassword();
      createOrUpdateUser(user, userDTO);
      user.setPassword(passTemp);

      userRepository.save(user);
      
      return Constants.EPOS_SUCCESS;
    } else {
      return Constants.EPOS_FALSE;
    }
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = Throwable.class)
  public String deleteUser(Long userId) {
    if (userId == null) {
      return Constants.EPOS_ISNULL;
    }
    User user = userRepository.findOne(userId);
    if (user != null) {
      userRepository.save(user);
      return Constants.EPOS_SUCCESS;
    } else {
      return Constants.EPOS_NOT_EXIST;
    }
  }

  @Override
  public List<UserDTO> findUser(SearchCriterialUserDTO criterialUserDTO) {
    return userRepository.findUser(criterialUserDTO);
  }

  @Override
  public Long countUser(SearchCriterialUserDTO criterialUserDTO) {
    return userRepository.countFindUser(criterialUserDTO);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = Throwable.class)
  public String updateStatusUser(Long userId, Integer status) {
    if (userId == null || status == null) {
      return Constants.EPOS_ISNULL;
    }
    User user = userRepository.findOne(userId);
    if (user != null) {
      user.setStatus(status);
      userRepository.save(user);
      return Constants.EPOS_SUCCESS;
    } else {
      return Constants.EPOS_NOT_EXIST;
    }
  }

  @Override
  public User getUserByUserName(String userName) {
    return userRepository.checkUserExisted(userName);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public UserDTO getUserById(Long userId) {
    if (userId == null)
      return null;
    User user = userRepository.findOne(userId);
    if (user != null) {
      UserDTO userDTO = new UserDTO();
      BeanUtils.copyProperties(user, userDTO);
      return userDTO;
    }

    return null;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = Throwable.class)
  public String resetPassword(Long userId, String passNew) {
    User user = userRepository.findOne(userId);
    if (user != null) {
      user.setPassword(passNew);
      user.setLastChangedPassword(Calendar.getInstance().getTime());

      PasswordHistory passwordHistory = new PasswordHistory();
      passwordHistory.setCreatedDate(Calendar.getInstance().getTime());
      passwordHistory.setPassword(passNew);
      passwordHistory.setUsers(user);
      passwordHistoryRepository.save(passwordHistory);

      userRepository.save(user);
      return Constants.EPOS_SUCCESS;
    } else {
      return Constants.EPOS_NOT_EXIST;
    }
  }

  @Override
  public User verify(String username, String password) {
    User user = userRepository.checkUserExisted(username);
    if (user != null) {
      // TODO password in database is not encoded now.
      if (!user.getPassword().equals(password)) {
        throw new UsernameNotFoundException("password does not match!");
      }
    } else {
      throw new UsernameNotFoundException("username '" + username + "'is not found!");
    }
    return user;
  }

}
