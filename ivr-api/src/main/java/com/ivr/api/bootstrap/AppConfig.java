package com.ivr.api.bootstrap;

import java.util.Properties;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.spi.PersistenceProvider;
import javax.sql.DataSource;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateExceptionTranslator;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Application configuration class
 * 
 * @author hoa.vutien
 * 
 * $LastChangedRevision: 10638 $
 * $LastChangedDate: 2014-05-24 10:17:15 +0700 (Sat, 24 May 2014) $
 * $LastChangedBy: nam.tranduc $
 */

@Configuration
@EnableTransactionManagement
@ComponentScan("com.ivr.api")
@EnableJpaRepositories("com.ivr.api.repository")
@PropertySource("classpath:application.properties")
public class AppConfig {
  private static final String PROPERTY_NAME_DATABASE_DRIVER                 = "db.driver";
  private static final String PROPERTY_NAME_DATABASE_PASSWORD               = "db.password";
  private static final String PROPERTY_NAME_DATABASE_URL                    = "db.url";
  private static final String PROPERTY_NAME_DATABASE_USERNAME               = "db.username";
  private static final String PROPERTY_NAME_HIBERNATE_DIALECT               = "hibernate.dialect";
  private static final String PROPERTY_NAME_HIBERNATE_SHOW_SQL              = "hibernate.show_sql";
  private static final String PROPERTY_NAME_HIBERNATE_CHARSET               = "hibernate.connection.charSet";
  private static final String PROPERTY_NAME_PERSISTENCEUNITNAME             = "persistence.unitname";
  private static final String PROPERTY_NAME_ENTITYMANAGER_PACKAGES_TO_SCAN  = "entitymanager.packages.to.scan";

  @Resource
  private Environment env;


  @Bean
  public EntityManager entityManager(EntityManagerFactory entityManagerFactory) {
    return entityManagerFactory.createEntityManager();
  }

  @Bean
  public PersistenceProvider persistenceProvider() {
    return new HibernatePersistenceProvider();
  }

  @Bean
  public DataSource dataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(env.getRequiredProperty(PROPERTY_NAME_DATABASE_DRIVER));
    dataSource.setUrl(env.getRequiredProperty(PROPERTY_NAME_DATABASE_URL));
    dataSource.setUsername(env.getRequiredProperty(PROPERTY_NAME_DATABASE_USERNAME));
    dataSource.setPassword(env.getRequiredProperty(PROPERTY_NAME_DATABASE_PASSWORD));

    return dataSource;
  }

  @Bean
  public LocalSessionFactoryBean sessionFactory() {
    LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
    sessionFactoryBean.setDataSource(dataSource());
    sessionFactoryBean.setPackagesToScan(env.getRequiredProperty(PROPERTY_NAME_ENTITYMANAGER_PACKAGES_TO_SCAN));
    sessionFactoryBean.setHibernateProperties(buildProperties());

    return sessionFactoryBean;
  }

  private Properties buildProperties() {
    Properties properties = new Properties();
    properties.put(PROPERTY_NAME_HIBERNATE_DIALECT,   env.getRequiredProperty(PROPERTY_NAME_HIBERNATE_DIALECT));
    properties.put(PROPERTY_NAME_HIBERNATE_SHOW_SQL,  env.getRequiredProperty(PROPERTY_NAME_HIBERNATE_SHOW_SQL));
    properties.put(PROPERTY_NAME_HIBERNATE_CHARSET,   env.getRequiredProperty(PROPERTY_NAME_HIBERNATE_CHARSET));

    return properties;
  }

  @Bean
  public JpaTransactionManager transactionManager() {
    JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
    jpaTransactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
    return jpaTransactionManager;
  }

  @Bean
  public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
    return new PersistenceExceptionTranslationPostProcessor();
  }

  @Bean
  public HibernateExceptionTranslator hibernateExceptionTranslator() {
    return new HibernateExceptionTranslator();
  }
  
  @Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setShowSql(true);
		vendorAdapter.setGenerateDdl(true);

		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();

		entityManagerFactoryBean.setJpaVendorAdapter(vendorAdapter);
		entityManagerFactoryBean.setDataSource(dataSource());
		entityManagerFactoryBean
				.setPackagesToScan(env
						.getRequiredProperty(PROPERTY_NAME_ENTITYMANAGER_PACKAGES_TO_SCAN));
		entityManagerFactoryBean.setPersistenceProvider(persistenceProvider());
		entityManagerFactoryBean.setPersistenceUnitName(env
				.getRequiredProperty(PROPERTY_NAME_PERSISTENCEUNITNAME));

		Properties jpaProperties = buildProperties();
		entityManagerFactoryBean.setJpaProperties(jpaProperties);
		entityManagerFactoryBean.afterPropertiesSet();
		return entityManagerFactoryBean;
	}




}
